import os, subprocess
from random import shuffle
from pathlib import Path

home_dir = str(Path.home())

def get_random(dir) -> str:
    batch : list = []
    for file in dir:
        batch.append(file)
    shuffle(batch)
    return str(batch[0])

def play_random(dir) -> None:
    s = get_random(os.listdir((dir)))
    subprocess.run(['xdg-open', dir+s])

def main():
    dir = home_dir + '/Music/'
    play_random(dir)
    
if __name__ == '__main__':
    main()